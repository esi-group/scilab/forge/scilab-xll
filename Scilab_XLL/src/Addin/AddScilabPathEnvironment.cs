﻿/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan CORNET
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
//=============================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
//=============================================================================
namespace DotNetScilab
{
    public class AddScilabPathEnvironment
    {
        /// <summary>
        /// Add a directory to current PATH environment variable
        /// </summary>
        /// <param name="ScilabPath">string directory to add</param>
        public static void Add(string ScilabPath)
        {
            string PATH = Environment.GetEnvironmentVariable("PATH");
            string newPATH = ScilabPath + ";" + PATH;
            Environment.SetEnvironmentVariable("PATH", newPATH);
        }
    }
}
//=============================================================================
