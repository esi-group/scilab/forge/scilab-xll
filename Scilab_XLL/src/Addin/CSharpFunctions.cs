﻿/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan CORNET
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
//=============================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using xlwDotNet;
using xlwDotNet.xlwTypes;
using DotNetScilab;
//=============================================================================
namespace Example
{
    public class ScilabFromExcel
    {
        static Scilab m_oSCilab = new Scilab(true);
        //=============================================================================
        [ExcelExport("Evaluate command in Scilab software")]
        public static int SLEvalString([Parameter("command to send to Scilab")] string command)
        {
            return m_oSCilab.SendScilabJob(command);
        }
        //=============================================================================
        [ExcelExport("Create or overwrite Scilab matrix with data from Microsoft Excel worksheet")]
        public static int SLPutMatrix([Parameter("variable name")] string variable_name,
                                            [Parameter("data")] CellMatrix matrixvalues)
        {
            Type CellType = matrixvalues[0, 0].GetType();
            bool isSameType = true;

            for (int i = 0; i < matrixvalues.RowsInStructure; i++)
            {
                for (int j = 0; j < matrixvalues.ColumnsInStructure; j++)
                {
                    if (matrixvalues[i, j].GetType() != CellType)
                    {
                        isSameType = false;
                        break;
                    }
                }
            }

            if (isSameType)
            {
                if (matrixvalues[0, 0].IsAString)
                {
                    string[] matrixString = new string[matrixvalues.RowsInStructure * matrixvalues.ColumnsInStructure];
                    if (matrixString != null)
                    {
                        for (int i = 0; i < matrixvalues.RowsInStructure; i++)
                        {
                            for (int j = 0; j < matrixvalues.ColumnsInStructure; j++)
                            {
                                matrixString[j * matrixvalues.RowsInStructure + i] = matrixvalues[i, j].StringValue();
                            }
                        }
                        return m_oSCilab.createNamedMatrixOfString(variable_name, matrixvalues.RowsInStructure, matrixvalues.ColumnsInStructure, matrixString);
                    }
                }

                if (matrixvalues[0, 0].IsANumber)
                {
                    double[] matrixDouble = new double [matrixvalues.RowsInStructure * matrixvalues.ColumnsInStructure];
                    if (matrixDouble != null)
                    {
                        for (int i = 0; i < matrixvalues.RowsInStructure; i++)
                        {
                            for (int j = 0; j < matrixvalues.ColumnsInStructure; j++)
                            {
                                matrixDouble[j * matrixvalues.RowsInStructure + i] = matrixvalues[i, j].NumericValue();
                            }
                        }
                        return m_oSCilab.createNamedMatrixOfDouble(variable_name, matrixvalues.RowsInStructure, matrixvalues.ColumnsInStructure, matrixDouble);
                    }
                }

                if (matrixvalues[0, 0].IsBoolean)
                {
                    bool[] matrixBoolean = new Boolean[matrixvalues.RowsInStructure * matrixvalues.ColumnsInStructure];
                    if (matrixBoolean != null)
                    {
                        for (int i = 0; i < matrixvalues.RowsInStructure; i++)
                        {
                            for (int j = 0; j < matrixvalues.ColumnsInStructure; j++)
                            {
                                matrixBoolean[j * matrixvalues.RowsInStructure + i] = (bool)(matrixvalues[i, j].BooleanValue());
                            }
                        }
                        return m_oSCilab.createNamedMatrixOfBoolean(variable_name, matrixvalues.RowsInStructure, matrixvalues.ColumnsInStructure, matrixBoolean);
                    }
                }
            }
            return -1;
        }
        //=============================================================================
        [ExcelExport("Write contents of Scilab matrix to Microsoft Excel worksheet")]
        public static CellMatrix SLGetMatrix([Parameter("variable name")] string variable_name)
        {
            int[] DimMatrix = m_oSCilab.getNamedVarDimension(variable_name);
            CellMatrix MatrixFromScilab = new CellMatrix(DimMatrix[0], DimMatrix[1]);

            switch (m_oSCilab.getNamedVarType(variable_name))
            {
                case (int)DotNetScilab.ScilabType.sci_strings:
                    {
                        String[] MatrixString = m_oSCilab.readNamedMatrixOfString(variable_name);

                        for (int i = 0; i < MatrixFromScilab.RowsInStructure; i++)
                        {
                            for (int j = 0; j < MatrixFromScilab.ColumnsInStructure; j++)
                            {
                                MatrixFromScilab[i, j] = new CellValue(MatrixString[j * MatrixFromScilab.RowsInStructure + i]);
                            }
                        }
                    }
                break;

                case (int)DotNetScilab.ScilabType.sci_matrix:
                    {
                        if (m_oSCilab.isNamedVarComplex(variable_name) == 1)
                        {
                            double[] MatrixDoubleRealPart = m_oSCilab.readNamedComplexMatrixOfDoubleRealPart(variable_name);
                            double[] MatrixDoubleImgPart = m_oSCilab.readNamedComplexMatrixOfDoubleImgPart(variable_name);

                            for (int i = 0; i < MatrixFromScilab.RowsInStructure; i++)
                            {
                                for (int j = 0; j < MatrixFromScilab.ColumnsInStructure; j++)
                                {
                                    string cmplx = MatrixDoubleRealPart[j * MatrixFromScilab.RowsInStructure + i].ToString() + '+' +
                                        MatrixDoubleImgPart[j * MatrixFromScilab.RowsInStructure + i].ToString() + 'i';

                                    MatrixFromScilab[i, j] = new CellValue(cmplx);
                                }
                            }
                        }
                        else
                        {
                            double[] MatrixDouble = m_oSCilab.readNamedMatrixOfDouble(variable_name);

                            for (int i = 0; i < MatrixFromScilab.RowsInStructure; i++)
                            {
                                for (int j = 0; j < MatrixFromScilab.ColumnsInStructure; j++)
                                {
                                    MatrixFromScilab[i, j] = new CellValue(MatrixDouble[j * MatrixFromScilab.RowsInStructure + i]);
                                }
                            }
                        }
                    }
                break;

                case (int)DotNetScilab.ScilabType.sci_boolean:
                    {
                        bool[] MatrixBoolean = m_oSCilab.getNamedMatrixOfBoolean(variable_name);

                        for (int i = 0; i < MatrixFromScilab.RowsInStructure; i++)
                        {
                            for (int j = 0; j < MatrixFromScilab.ColumnsInStructure; j++)
                            {
                                MatrixFromScilab[i, j] = new CellValue((bool)(MatrixBoolean[j * MatrixFromScilab.RowsInStructure + i]));
                            }
                        }
                    }
                break;

            }
            return MatrixFromScilab;
        }
    }
}
//=============================================================================
